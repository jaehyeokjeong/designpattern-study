package iterator3;

import java.util.Iterator;

public class Waitress {
	Menu pancakeHouseMenu = new PancakeHouseMenu();
	Menu dinerMenu = new DinerMenu();
	//메뉴가 추가되어야함
	
	public void printMenu() {
		Iterator<MenuItem> pancakeIterator = pancakeHouseMenu.createIterator();
		Iterator<MenuItem> dinerIterator = dinerMenu.createIterator();
		
		printMenu(pancakeIterator);
		
		System.out.println("");
		
		printMenu(dinerIterator);
		
		//추가된 메뉴의 이터레이터를 받아서 메뉴를 출력하자
	}
	
	//구현에 변경이 없다.
	private void printMenu(Iterator<MenuItem> iterator) {
		while (iterator.hasNext()) {
			MenuItem menuItem = (MenuItem)iterator.next();
			System.out.print(menuItem.getName() + ", ");
			System.out.print(menuItem.getPrice() + " -- ");
			System.out.println(menuItem.getDescription() + " ");
		}
	}
}
