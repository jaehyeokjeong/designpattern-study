package composite;

public class MenuItem extends MenuComponent {
	String name;
	String description;
	boolean vegetarian;
	double price;

	public MenuItem(String name, String description, boolean vegetarian, double price) {
		this.name = name;
		this.description = description;
		this.vegetarian = vegetarian;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public double getPrice() {
		return price;
	}

	public boolean isVegetarian() {
		return vegetarian;
	}
	
	// 기존에 비해 변화한 부분
	// MenuComponent의 print()메소드를 오버라이드 한다.
	public void print() {
		System.out.print(getName());
		if (isVegetarian()) {
			System.out.print("(v), ");
		}
		System.out.print(getPrice() + " -- ");
		System.out.println(getDescription() + " ");
	}
}
