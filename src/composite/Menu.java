package composite;

import java.util.ArrayList;
import java.util.Iterator;

// Menu도 MenuComponenet를 상속한다.
public class Menu extends MenuComponent {
	//Menu는 MenuComponent를 몇 개든 저장할 수 있다.
	ArrayList<MenuComponent> menuComponents = new ArrayList<>();
	String name;
	String description;

	public Menu(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	//Menu, MenuItem 모두 MenuComponent를 상속받았으므로 이 메소드들을 가지고 모두 처리 가능
	public void add(MenuComponent menuComponent) {
		menuComponents.add(menuComponent);
	}

	public void remove(MenuComponent menuComponent) {
		menuComponents.remove(menuComponent);
	}

	public MenuComponent getChild(int i) {
		return menuComponents.get(i);
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public void print() {
		System.out.print("\n" + getName());
		System.out.print(", " + getDescription());
		System.out.println("------------------------------------------");
		
		// Menu 정보뿐만 아니라 Menu안의 Item까지 출력
		Iterator<MenuComponent> iterator = menuComponents.iterator(); 
		while (iterator.hasNext()) {
			MenuComponent menuComponent = iterator.next();
			menuComponent.print();
		}
	}
}