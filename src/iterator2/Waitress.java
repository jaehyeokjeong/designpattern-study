package iterator2;

import java.util.Iterator;

public class Waitress {
	Menu pancakeHouseMenu = new PancakeHouseMenu();
	Menu dinerMenu = new DinerMenu();
	
	public void printMenu() {
		Iterator<MenuItem> pancakeIterator = pancakeHouseMenu.createIterator();
		Iterator<MenuItem> dinerIterator = dinerMenu.createIterator();
		
		printMenu(pancakeIterator);
		
		System.out.println("");
		
		printMenu(dinerIterator);
	}
	
	private void printMenu(Iterator<MenuItem> iterator) {
		while (iterator.hasNext()) {
			MenuItem menuItem = (MenuItem)iterator.next();
			System.out.print(menuItem.getName() + ", ");
			System.out.print(menuItem.getPrice() + " -- ");
			System.out.println(menuItem.getDescription() + " ");
		}
	}
}
