package composite3;

import java.util.Iterator;
import java.util.Stack;

public class CompositeIterator implements Iterator<MenuComponent> {
	Stack<Iterator<MenuComponent>> stack = new Stack<>();

	//4개의 메소드를 구현하여 Composite Iterator를 완성해야 한다.
	public CompositeIterator(Iterator<MenuComponent> iterator) {
		
	}

	public MenuComponent next() {
		
	}

	public boolean hasNext() {
		
	}

	public void remove() {
		
	}
}